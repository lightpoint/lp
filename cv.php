<?php include "header.php"; ?>

<main>
<div class="container">
  <div class="row">
    <div class="col-xs-12"><p>&nbsp;</p><h3>CV Łukasz Warguła</h3>
      <p>Data urodzenia: 13.04.1979<br/>Adres email: <a href="mailto:lwargula@gmail.com">lwargula@gmail.com</a><br/><a href="files/CV_Lukasz_Wargula.pdf" target="_blank">wersja PDF</a></p>
    </div>
  </div>
  
  
  <div class="row">
    <div class="col-xs-12">
      <p>Języki: PHP, Delphi<br/>
        javascript, jQuery, JSON, CSS, LESS, SCSS, Angular, Bootstrap, Materialize, Ajax<br/>
        Frameworki: Kohana, Silex, Light<br/>
        Bazy: MySQL, MSSQL, InterBase, FireBird, SyBase<br/>
        Inne: podstawy obsługi programów graficznych(PS, Fireworks , Gimp), praca z SVN (obecnie głównie BitBucket + SourceTree), Wordpress, Joomla, podstawy Symfony i Laravel, podstawy Pythona</p>
      </div>
  </div>
</div>
  
<div class="linespacer"></div>

<div class="container">
  <div class="row">
    <div class="col-xs-12"><h3>Edukacja</h3></div>
  </div>
  
  <div class="row">
    <div class="col-xs-12 col-md-3"><p>2000 - 2002</p></div>
    <div class="col-xs-12 col-md-9">
      <p>Wyższa Szkoła Komunikacji i Zarządzania, specjalność informatyka;</p>
    </div>
  </div>
  
  <div class="row">
    <div class="col-xs-12 col-md-3"><p>1998 - 2000</p></div>
    <div class="col-xs-12 col-md-9">
      <p>Policealne Studium Informatyczne przy Zespole Szkół Zawodowych we Wronkach;</p>
    </div>
  </div>
  		
    <div class="row">
    <div class="col-xs-12 col-md-3"><p>1994 - 1998</p></div>
    <div class="col-xs-12 col-md-9">
      <p>Liceum Ogólnokształcące w Szamotułach;</p>
    </div>
  </div>       	
	
  
</div>

<div class="linespacer"></div>

<div class="container">
  <div class="row">
    <div class="col-xs-12"><h3>Doświadczenie zawodowe</h3></div>
  </div>
  
  <div class="row">
    <div class="col-xs-12 col-md-3"><p>od 04.2012</p></div>
    <div class="col-xs-12 col-md-9">
      <p><strong>Expansja Advertising Sp. z o.o.</strong></p>
      <p>PHP Developer - projektowanie, wdrażanie i utrzymanie progrmaów lojalnościowych, serwisów opartych o platformy open-source (Wordpress, Joomla), oraz własne rozwiązania</p>
    </div>
  </div>
  
  <div class="row">
    <div class="col-xs-12 col-md-3"><p>08.2008 - 01.2012</p></div>
    <div class="col-xs-12 col-md-9">
      <p><strong>Kemo.pl</strong></p>
      <p>PHP Developer - wdrażanie serwisów e-commerce oraz rozwijanie platformy sprzedażowej (domeny i usługi)</p>
    </div>
  </div>
  
  <div class="row">
    <div class="col-xs-12 col-md-3"><p>02.2008 - 07.2008</p></div>
    <div class="col-xs-12 col-md-9">
      <p><strong>Coyote S.A</strong></p>
      <p>PHP Developer - serwisy internetowe - wdrażanie i utrzymanie, rozwijanie autorskiego systemu e-commerce</p>
    </div>
  </div>
  
  <div class="row">
    <div class="col-xs-12 col-md-3"><p>09.2005 - 02.2008</p></div>
    <div class="col-xs-12 col-md-9">
      <p><strong>Trol Intermedia</strong></p>
      <p>Full Stack Developer - serwisy internetowe i e-commerce wdrażanie i utrzymanie, rozwijanie systemu 2ClickShop</p>
    </div>
  </div>
  
  <div class="row">
    <div class="col-xs-12 col-md-3"><p>11.2004 - 09.2005</p></div>
    <div class="col-xs-12 col-md-9">
      <p><strong>EuroSoft</strong></p>
      <p>Delphi Developer - rozwijanie i utrzymanie platformy sprzedażowej ERP</p>
    </div>
  </div>
  
  <div class="row">
    <div class="col-xs-12 col-md-3"><p>01.2000 - 01.2003</p></div>
    <div class="col-xs-12 col-md-9">
      <p><strong>Systemy Informatyczne JCom</strong></p>
      <p>serwis sprzętowy, sieci i programowanie Delphi</p>
    </div>
  </div>
</div>
  
<div class="linespacer"></div>

<div class="container">
  <div class="row">
    <div class="col-xs-12"><h3>Zainteresowania</h3></div>
  </div>
  
  <div class="row">
    <div class="col-xs-12">
      <ul>
        <li>programowanie, gamedev, systemy mobilne</li>
        <li>elektronika analogowa i cyfrowa</li>
        <li>muzyka, wszystko co związane z gitarą elektryczną</li>
        <li>film i literatura (s-f)</li>
        <li>turystyka piesza i rowerowa, fotografia</li>
      </ul>
    </div>
  </div>
  
</div>
</main>
<?php include "footer.php"; ?>