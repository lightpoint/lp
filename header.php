<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="description" content="test">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <meta http-equiv="Pragma" content="no-cache, private" />
  <meta http-equiv="Expires" content="0" />
  <meta http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate" />
  <meta http-equiv="Cache-Control" content="post-check=0, pre-check=0" />
  
  <title>LightPoint</title>
  <link href="css/style.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=latin-ext" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <!--<script src="js/bootstrap.min.js"></script>
  <script src="js/script.js"></script>-->
</head>
<body>
	<div id="nav">
		<div class="container">
			<div class="nav-logo"><a href="index.php"><img src="img/lightpoint.png"></a></div>
			<div class="nav-icon">
				<div class="nav-icon-container">
					<div class="nav-icon-line"></div>
				</div>
			</div>
			<div class="nav-container">
				<ul>
                    <li id=""><a href="index.php">PORTFOLIO</a></li>
					<li id=""><a href="cv.php">CV</a></li>
					<li id=""><a href="kontakt.php">KONTAKT</a></li>
				</ul>
			</div>
		</div>
	</div>