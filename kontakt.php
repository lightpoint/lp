<?php include "header.php"; ?>

<main>
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <p>&nbsp;</p>
      <h4>Łukasz Warguła</h4>
      <h5>programista PHP</h5>
      <p><strong>Poznań</strong></p>
      <p>Adres email: <a href="mailto:lwargula@gmail.com">lwargula@gmail.com</a><br/><a href="files/CV_Lukasz_Wargula.pdf" target="_blank">CV wersja PDF</a></p></p>
    </div>
  </div>
</div>
</main>
<?php include "footer.php"; ?>