<?php include "header.php"; ?>

<main>

<!--JURAJSKA-->
<div class="page">
  <div class="col-xs-12 col-md-9 page-element  page-element-background page-element-background-jurajska"></div>
  <div class="col-xs-12 col-md-3 page-element page-element-details">
    <div class="page-details-title"><h3>Jurajska</h3></div>
    <p><strong>PHP/HTML/CSS/JS</strong></p>
    <ul>
      <li>wdrożenie html/css/js do szablonów cms</li>
      <li>zaprojektowanie i wdrożenie UI</li>
    </ul>
    <p><a href="http://jurajska.pl/" target="_blank">www.jurajska.pl</a></p>
  </div>
</div>
  
<div class="page-spacer"></div>

<!--ATRIGRAN-->
<div class="page">
  <div class="col-xs-12 col-md-3 page-element page-element-details">
    <div class="page-details-title"><h3>Atrigran</h3></div>
    <p><strong>PHP/HTML/CSS/JS</strong></p>
    <ul>
      <li>wdrożenie html/css/js do szablonów cms</li>
      <li>zaprojektowanie i wdrożenie UI</li>
    </ul>
    <p><a href="http://atrigran.com/" target="_blank">www.atrigran.com</a></p>
  </div>
  <div class="col-xs-12 col-md-9 page-element  page-element-background page-element-background-atrigran"></div>
</div>
  
<div class="page-spacer"></div>
  
<!--AVENIDA-->
<div class="page">
  <div class="col-xs-12 col-md-9 page-element  page-element-background page-element-background-avenida"></div>
  <div class="col-xs-12 col-md-3 page-element page-element-details">
    <div class="page-details-title"><h3>PCC/Avenida</h3></div>
    <p><strong>PHP/HTML/CSS/JS</strong></p>
    <ul>
      <li>zaprojektowanie i wdrożenie systemu wydawania kart bonusowych</li>
      <li>stworzenie i wdrożenie pluginu Wordpress pozwalającego rejestrować karty on-line</li>
      <li>stworzenie i wdrożenie panelu administracyjnego do rejestrowania, wydruku i wydawania kart bonusowych dla klientów galerii</li>
      <li>wdrożenie systemu mailingowego dla bazy klientów</li>
      <li>skrypty raportujące</li>
    </ul>
    <p><a href="http://avenidapoznan.com/" target="_blank">www.avenidapoznan.com</a></p>
  </div>
</div>
  
<div class="page-spacer"></div>
  
<!--BEABELEZA-->
<div class="page">
  <div class="col-xs-12 col-md-3 page-element page-element-details">
    <div class="page-details-title"><h3>BeaBonus Beabeleza</h3></div>
    <p><strong>PHP/HTML/CSS/JS</strong></p>
    <ul>
      <li>zaprojektowanie i wdrożenie programu lojalnościowego dla hurtowni branży fryzjerskiej</li>
      <li>system oparty o synchronizację z programem księgowym</li>
      <li>zaprojektowanie i wdrożenie modułów obsługujących obsługę i pozyskiwanie klientów, obsługę zamówień i wysyłek nagród, przepływ dokumentów, raportowania, newsletterów, powiadomień sms</li>
    </ul>
    <p><a href="http://www.beabonus.beabeleza.pl/" target="_blank">www.beabonus.beabeleza.pl</a></p>
  </div>
  <div class="col-xs-12 col-md-9 page-element  page-element-background page-element-background-beabeleza"></div>
</div>
  
<div class="page-spacer"></div>

<!--MAKEWONDER-->
<div class="page">
  <div class="col-xs-12 col-md-9 page-element  page-element-background page-element-background-makewonder"></div>
  <div class="col-xs-12 col-md-3 page-element page-element-details">
    <div class="page-details-title"><h3>Makewonder</h3></div>
    <p><strong>PHP/HTML/CSS/JS</strong></p>
    <ul>
      <li>Przeniesienie, dostosowanie i przeprojektowanie amerykańskiej wersji makewonder.com na system cms</li>
    </ul>
    <p><a href="http://www.makewonder.pl/" target="_blank">www.makewonder.pl</a></p>
  </div>
</div>
  
<div class="page-spacer"></div>
  
<!--WONDERPOLSA-->
<div class="page">
  <div class="col-xs-12 col-md-3 page-element page-element-details">
    <div class="page-details-title"><h3>Wonder Polska</h3></div>
    <p><strong>HTML/CSS/JS</strong></p>
    <ul>
      <li>Sklep dla wonderpolska postawiony na systemie IAI Shop</li>
    </ul>
    <p><a href="http://www.wonderpolska.pl/" target="_blank">www.wonderpolska.pl</a></p>
  </div>
  <div class="col-xs-12 col-md-9 page-element  page-element-background page-element-background-wonderpolska"></div>
</div>
  
<div class="page-spacer"></div>

<!--ONEMOVE-->
<div class="page">
  <div class="col-xs-12 col-md-9 page-element  page-element-background page-element-background-onemove"></div>
  <div class="col-xs-12 col-md-3 page-element page-element-details">
    <div class="page-details-title"><h3>One Move</h3></div>
    <p><strong>PHP/HTML/CSS/JS</strong></p>
    <ul>
      <li>Zaprojektowanie UI, wdrożenie szablonu i odpowiednich skryptów js</li>
      <li>wykonanie animacji w Google Web Designer</li>
    </ul>
    <p><a href="http://www.onemove.com.pl/" target="_blank">www.onemove.com.pl</a></p>
  </div>
</div>
  
<div class="page-spacer"></div>
  
<!--LUVENA-->
<div class="page">
  <div class="col-xs-12 col-md-3 page-element page-element-details">
    <div class="page-details-title"><h3>Nawozy Luvena</h3></div>
    <p><strong>PHP/HTML/CSS/JS</strong></p>
    <ul>
      <li>Wdrożenie szablonu Wordpress</li>
      <li>Stworzenie pluginów integrujących wyszukiwarkę dystrybutorów z Google Maps</li>
    </ul>
    <p><a href="http://www.nawozy.pl/" target="_blank">www.nawozy.pl</a></p>
  </div>
  <div class="col-xs-12 col-md-9 page-element  page-element-background page-element-background-luvena"></div>
</div>
  
</main>
<?php include "footer.php"; ?>